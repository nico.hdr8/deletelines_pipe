package pipe;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

/**
 * Created: 02.03.2023
 *
 * @author Nico Haider (20200527)
 */
public class Pipe {
    public static void main(String[] args) {
        try (PipedInputStream inputStream = new PipedInputStream();
             PipedOutputStream outputStream = new PipedOutputStream();
             FileOutputStream fileOutputStream = new FileOutputStream("src/main/resources/pipeOutput.txt")) {

            inputStream.connect(outputStream);

            StreamCopier.copyByte(System.in, outputStream);

            while (inputStream.available() >= 4) {
                StreamCopier.copyBuffered(inputStream, fileOutputStream, 4);
            }

            int available = inputStream.available();
            if (available > 0) {
                StreamCopier.copyBuffered(inputStream, fileOutputStream, available);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
