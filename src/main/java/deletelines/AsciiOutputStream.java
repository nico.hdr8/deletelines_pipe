package deletelines;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created: 27.02.2023
 *
 * @author Nico Haider (20200527)
 */
public class AsciiOutputStream extends FileOutputStream {
    public AsciiOutputStream(String filename) throws FileNotFoundException {
        super(filename);
    }

    public void writeLine(List<Integer> line) throws IOException {
        for (int current : line) {
            super.write(current);
        }
        super.write(10);
    }
}
