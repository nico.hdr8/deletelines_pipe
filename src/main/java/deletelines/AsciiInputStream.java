package deletelines;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created: 27.02.2023
 *
 * @author Nico Haider (20200527)
 */
public class AsciiInputStream extends FileInputStream {
    public AsciiInputStream(String filename) throws FileNotFoundException {
        super(filename);
    }

    public List<Integer> readLine() throws IOException {
        List<Integer> inputs = new ArrayList<>();
        int current = super.read();
        while (current != 10 && current != -1) {
            inputs.add(current);
            current = super.read();
        }
        return inputs;
    }

    public void skipLine() throws IOException {
        readLine();
    }
}
