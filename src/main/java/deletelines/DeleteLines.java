package deletelines;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

/**
 * Created: 27.02.2023
 *
 * @author Nico Haider (20200527)
 */
public class DeleteLines {
    public static void main(String[] args) {
        if (args.length < 3) {
            throw new IllegalArgumentException("have to be at least 3 arguments");
        }

        try (AsciiInputStream inputStream = new AsciiInputStream(args[0]); AsciiOutputStream outputStream = new AsciiOutputStream(args[1])) {
            String[] lineIndex = Arrays.copyOfRange(args, 2, args.length);
            Set<Integer> toSkip = new TreeSet<>();

            for (String current : lineIndex) {
                String[] split = current.split("-");
                if (split.length == 2) {
                    for (int i = Integer.parseInt(split[0]); i <= Integer.parseInt(split[1]); i++) {
                        toSkip.add(i);
                    }
                } else if (split.length == 1) {
                    toSkip.add(Integer.parseInt(split[0]));
                }
            }

            System.out.println(toSkip);

            int index = 1;
            List<Integer> currentLine = inputStream.readLine();
            while (!currentLine.isEmpty()) {
                if (!toSkip.contains(index)) {
                    outputStream.writeLine(currentLine);
                }
                currentLine = inputStream.readLine();
                index++;
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
